import numpy as np
import networkx as nx
import matplotlib.pyplot as plt

# Define Visualization functions

def community_filter(labels, community, scale):
    community_filter = np.ones(labels.size, dtype='i4')
    community_filter[np.argwhere(labels == community)] = scale
    return community_filter


def visualize(G, pos, labels, title):
    plt.figure(figsize=(30,30))
    nx.draw(G, layout=pos, node_size=100, node_color=labels, cmap=plt.cm.gist_rainbow)
    plt.title(title, fontsize=60)


def visualize_community(G, pos, labels, community, title):
    plt.figure(figsize=(30,30))
    department_filter = community_filter(labels, community, scale=5)
    highlight_nodes = 100*department_filter
    # edges = G.edges(np.squeeze(np.argwhere(labels == community).T))
    nx.draw(G, layout=pos, node_size=highlight_nodes, node_color=department_filter, cmap=plt.cm.bwr)
    #  nx.draw_networkx_edges(G, pos=pos, edgelist=edges, edge_color='red', width=5)
    plt.title(title, fontsize=60)
    

def draw(G, pos, measures, measure_name):
    plt.figure(figsize=(30,30))
    nodes = nx.draw_networkx_nodes(G, pos, node_size=250, cmap=plt.cm.plasma, 
                                   node_color=measures.values(),
                                   nodelist=list(measures.keys()))
    nodes.set_norm(mcolors.SymLogNorm(linthresh=0.01, linscale=1))
    
    # labels = nx.draw_networkx_labels(G, pos)
    edges = nx.draw_networkx_edges(G, pos)

    plt.title(measure_name)
    plt.colorbar(nodes)
    plt.axis('off')
    plt.show()
    

def highlight_node_and_neighbors(G, pos, node):
    plt.figure(figsize=(30,30)) 
    nx.draw(G, layout=pos, node_size=100, node_color='gray')
    edges = G.edges(node)
    neighbors = G.neighbors(node)
    nx.draw_networkx_nodes(G, pos=pos, nodelist=list(G.neighbors(node)), node_color='green', node_size=250)
    nx.draw_networkx_edges(G, pos=pos, edgelist=edges, edge_color='red', width=5)
    nx.draw_networkx_nodes(G, pos=pos, nodelist=[node], node_color='blue', node_size=1000)
    plt.axis('equal')
    plt.title('Node %d & Neighbors' % node, fontsize=60)
    plt.show()
    
### Define Community <--> Label Conversion Functions

# Function takes a 1D numpy array of labels and converts it to a 2D python list of communities
# * Note that each community in the resulting array can have a unique size, so the 2D array is not a matrix
# * * I'd like to figure out how to get type checking out of this
def convert_labels_to_communities(labels):
    communities = []
    community_labels = np.unique(labels)
    if community_labels.size == 0:
        return [np.arange(labels.size).tolist()]
    else:
        for community_label in np.unique(labels):
            nodes_of_community = np.squeeze(np.argwhere(labels == community_label).T)
            nodes_list = nodes_of_community.tolist()
            if isinstance(nodes_list, int):
                communities.append([nodes_list])
            else:
                communities.append(nodes_list)
        return communities
    
# Function takes a 2D python list of communities and converts it to a 1D numpy array of labels
def convert_communities_to_labels(G, communities):
    labels = []
    for node in G.nodes():
        for i in range(len(communities)):
            if node in communities[i]:
                labels.append(i)
                continue
    return np.array(labels)