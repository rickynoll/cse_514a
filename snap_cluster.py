#!/usr/bin/env python
import snap
import numpy as np
import time
import sys


G_email = snap.LoadEdgeList(snap.PUNGraph, './data/email/email-Eu-core.txt')
# for edge in G_email.Edges():
#     print egde.GetId()


# for NI in G_email.Nodes():
#     print NI.GetId()

print(' * * * * * Feedback * * * * * ')

CmtyV = snap.TCnComV()

t0 = time.clock()

modularity = snap.CommunityGirvanNewman(G_email, CmtyV)
t1 = time.clock()
running_time = t1 - t0

print(' * * * * * Feedback * * * * * ')


for Cmty in CmtyV:
	sys.stdout.write('\n')
	for NI in Cmty:
		sys.stdout.write('%d,' % NI)


print "The modularity of the network is %f" % modularity
print 'The script ran in %f seconds' % running_time


with open('email_betweeness_modularity.txt', 'w') as f:
  f.write('%f' % modularity)

with open('email_betweeness_timing.txt', 'w') as f:
  f.write('%f' % running_time)


communities = np.array([[node for node in Cmty] for Cmty in CmtyV])
np.savetxt('email_betweeness_communities.txt', communities)


print(' * * * * * DONE! * * * * * ')
